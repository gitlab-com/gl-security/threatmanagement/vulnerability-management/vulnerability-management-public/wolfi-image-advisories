/*
Copyright © 2023 GitLab
*/
package main

import (
	"gitlab.com/gitlab-com/gl-security/product-security/vulnerability-management/vulnerability-management-public/wolfi-image-advisories/cmd"
)

func main() {
	cmd.Execute()
}
