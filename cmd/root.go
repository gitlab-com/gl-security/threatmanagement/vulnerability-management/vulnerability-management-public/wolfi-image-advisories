/*
Copyright © 2023 GitLab
*/
package cmd

import (
	"fmt"
	"os"

	"gitlab.com/gitlab-com/gl-security/product-security/vulnerability-management/vulnerability-management-public/wolfi-image-advisories/pkg/cli"

	"github.com/spf13/cobra"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	imageUrl          string
	imageArch         string
	imageOS           string
	wolfiAdvisoryRepo string
	logLevel          string
)

const (
	defaultWolfiAdvisoryRepo string = "https://github.com/wolfi-dev/advisories"
	imageUrlPFlag            string = "image"
	logLevelPFlag            string = "log-level"
	imageArchPFlag           string = "image-arch"
	imageOSPFlag             string = "image-os"
	wolfiAdvisoryRepoPFlag   string = "wolfi-advisory-repo"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "wolfi-image-advisories",
	Short: "Lists advisories for a container image build from wolfi/chainguard images",
	Long: `Provides security advisory information about wolfi/chainguard container images.

Given an fully-qualified image path, this tool will:
 - Fetch the latest advisory data for Wolfi
 - Pull the tagged image version
 - Retreive the embedded SBOM files installed during build by wolfi apk packages
 - Parse out each package in the image, and output advisory data
`,
	Run: func(cmd *cobra.Command, args []string) {
		logger := zap.L()
		c, err := cli.NewCli(logger, imageUrl, imageArch, imageOS, wolfiAdvisoryRepo)
		if err != nil {
			os.Exit(1)
		}
		c.AdvisoryList()
	},
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initLogging)

	// Config for logging
	rootCmd.PersistentFlags().StringVarP(&logLevel, logLevelPFlag, "l", "info", "set log level (debug/info/warn/error)")

	// Image config
	rootCmd.PersistentFlags().StringVarP(&imageUrl, imageUrlPFlag, "i", "", "set image URL (required)")
	err := rootCmd.MarkPersistentFlagRequired(imageUrlPFlag)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to mark flag %s as required: %v\n", imageUrlPFlag, err)
		os.Exit(1)
	}
	rootCmd.PersistentFlags().StringVarP(&imageArch, imageArchPFlag, "a", "amd64", "set desired image architecture")
	rootCmd.PersistentFlags().StringVarP(&imageOS, imageOSPFlag, "o", "linux", "set desired image OS")

	// Advisory repo
	rootCmd.PersistentFlags().StringVarP(&wolfiAdvisoryRepo, wolfiAdvisoryRepoPFlag, "r", defaultWolfiAdvisoryRepo, "provide an alternate wolfi advisory repo")
}

// initLogging sets up zap with our logging config
func initLogging() {

	level := zap.InfoLevel
	switch logLevel {
	case "debug":
		level = zap.DebugLevel
	case "warning":
		level = zap.WarnLevel
	case "error":
		level = zap.ErrorLevel
	}
	encoder_config := zap.NewDevelopmentEncoderConfig()
	encoder_config.EncodeLevel = zapcore.CapitalColorLevelEncoder
	loggerConfig := zap.Config{
		Level:            zap.NewAtomicLevelAt(level),
		Encoding:         "console",
		EncoderConfig:    encoder_config,
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
	}

	logger, _ := loggerConfig.Build()
	// Logging
	defer logger.Sync()
	zap.ReplaceGlobals(logger)
}
