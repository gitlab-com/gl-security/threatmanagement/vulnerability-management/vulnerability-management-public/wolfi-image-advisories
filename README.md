# wolfi-image-advisories

lists advisory information for each SPDX SBOM package in an image built on wolfi/chainguard images
these SBOMs are included for all APK packages included in the build of chainguard & derivative images, including custom packages built with melange, so this tool can be used to get insight into the status & history for vulnerabilities impacting a given image.

## Usage
run `wolfi-image-advisories -i IMAGE_NAME` where `IMAGE_NAME` is a fully qualified image path, including tag.
this tool has been tested on chainguard published images, as well as derivative images built on top of wolfi.

## Features
- pulls image on each run, to ensure latest SBOM is used
- updates local cache of advisory data from [the wolfi advisory git repo](https://github.com/wolfi-dev/advisories)
- parses all SPDX SBOM files from the pulled image
- compares each package to the advisory data in the wolfi advisory repo
- outputs a list of advisories including:
  - image name
  - vulnerability ID (CVE)
  - package name
  - package version
  - advisory release status
  - advisory notes (action, justification, impact)

## Author
[James Hebden](https://gitlab.com/jhebden) @ GitLab Security

