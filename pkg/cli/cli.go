package cli

import (
	"fmt"

	"gitlab.com/gitlab-com/gl-security/product-security/vulnerability-management/vulnerability-management-public/wolfi-image-advisories/pkg/advisory"
	"gitlab.com/gitlab-com/gl-security/product-security/vulnerability-management/vulnerability-management-public/wolfi-image-advisories/pkg/image"

	"go.uber.org/zap"
)

const (
	RFC3339 string = "2006-01-02T15:04:05Z"
)

type Cli struct {
	Logger          *zap.Logger
	AdvisoryRepoURL string
	ImageURL        string
	ImageOS         string
	ImageArch       string
	AdvisoryRepo    *advisory.AdvisoryRepo
	Image           *image.WolfiImage
}

func (c *Cli) AdvisoryList() error {
	// update advisory data
	c.Logger.Sugar().Infof("updating advisory repo from %s", c.AdvisoryRepoURL)
	err := c.AdvisoryRepo.Update()
	if err != nil {
		c.Logger.Sugar().Errorf("failed to update advisory repo: %v", err)
		return err
	}
	// pull image
	c.Logger.Sugar().Infof("updating image %s for OS %s on arch %s", c.ImageURL, c.ImageOS, c.ImageArch)
	err = c.Image.Update()
	if err != nil {
		c.Logger.Sugar().Errorf("failed to download & process image: %v", err)
		return err
	}
	// get advisory for each package
	c.Logger.Sugar().Infof("listing advisories for image %s:", c.ImageURL)
	fmt.Printf("image,package name,package version,advisory id,status,justification,impact,action,fixed version,timestamp\n")
	for _, package_detail := range c.Image.Packages {
		advisory_details, err := c.AdvisoryRepo.GetAdvisoriesForPackageName(package_detail.PackageName)
		if err != nil {
			c.Logger.Sugar().Warnf("failed to get advisory for package %s: %v", package_detail.PackageName, err)
			continue
		}
		for _, advisory_detail := range advisory_details {
			for advisory_id, advisory_data := range advisory_detail. {
				for _, advisory_entry := range advisory_data {
					fmt.Printf("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n", c.ImageURL, package_detail.PackageName, package_detail.PackageVersion, advisory_id, advisory_entry.Status, advisory_entry.Justification, advisory_entry.ImpactStatement, advisory_entry.ActionStatement, advisory_entry.FixedVersion, advisory_entry.Timestamp.Format(RFC3339))
				}
			}
		}
	}
	return nil
}

func NewCli(logger *zap.Logger, image_url string, image_arch string, image_os string, advisory_repo_url string) (*Cli, error) {
	cli := Cli{
		Logger:          logger,
		ImageURL:        image_url,
		ImageOS:         image_os,
		ImageArch:       image_arch,
		AdvisoryRepoURL: advisory_repo_url,
	}
	advisory_repo, err := advisory.NewAdvisoryRepo(logger, advisory_repo_url)
	if err != nil {
		return &cli, err
	}
	cli.AdvisoryRepo = advisory_repo
	image, err := image.NewWolfiImage(logger, image_url, image_arch, image_os)
	if err != nil {
		return &cli, err
	}
	cli.Image = image
	return &cli, nil
}
