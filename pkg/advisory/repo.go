package advisory

import (
	"fmt"
	"os"
	"path/filepath"

	"go.uber.org/zap"

	"github.com/go-git/go-git/v5"
	"github.com/wolfi-dev/wolfictl/pkg/configs"
	advisoryconfigs "github.com/wolfi-dev/wolfictl/pkg/configs/advisory/v2"
	rwos "github.com/wolfi-dev/wolfictl/pkg/configs/rwfs/os"
)

type AdvisoryRepo struct {
	AdvisoryDir   string
	AdvisoryIndex *configs.Index[advisoryconfigs.Document]
	Logger        *zap.Logger
	URL           string
}

func (a *AdvisoryRepo) GetAdvisoriesForPackageName(package_name string) ([]advisoryconfigs.Document, error) {
	advisories := make([]advisoryconfigs.Document, 0)
	if a.AdvisoryIndex == nil {
		return advisories, fmt.Errorf("failed to get advisory for package %s: advisory index is not loaded", package_name)
	}
	advisories = a.AdvisoryIndex.Select().WhereName(package_name).Configurations()
	return advisories, nil
}

func (a *AdvisoryRepo) Update() error {
	// check if repo exists locally
	a.Logger.Sugar().Debugf("checking if advisory repo is checked out and up to date")
	if _, err := os.Stat(a.AdvisoryDir); os.IsNotExist(err) {
		// if not, clone it
		a.Logger.Sugar().Debugf("need to clone advisory repo, creating advisory directory")
		err := os.MkdirAll(a.AdvisoryDir, 0700)
		if err != nil {
			return fmt.Errorf("failed to make advisory directory: %v", err)
		}
		a.Logger.Sugar().Debugf("directory created, cloning repo %s into %", a.URL, a.AdvisoryDir)
		_, err = git.PlainClone(a.AdvisoryDir, false, &git.CloneOptions{
			URL:      a.URL,
			Progress: os.Stdout,
		})
		if err != nil {
			return fmt.Errorf("failed to clone wolfi advisory repo: %v", err)
		}
	} else {
		// if it does, pull latest changes
		a.Logger.Sugar().Debugf("advisory repo already exists, pulling latest changes")
		git_repo, err := git.PlainOpen(a.AdvisoryDir)
		if err != nil {
			return fmt.Errorf("failed to open existing advisory repo: %v", err)
		}
		git_tree, err := git_repo.Worktree()
		if err != nil {
			return fmt.Errorf("failed to get worktree for advisory repo: %v", err)
		}
		err = git_tree.Pull(&git.PullOptions{RemoteName: "origin"})
		if err != nil && err != git.NoErrAlreadyUpToDate {
			return fmt.Errorf("failed to pull latest changes from advisory repo origin: %v", err)
		}
	}
	a.Logger.Sugar().Debugf("advisory repo up to date")
	// load using wolfictl handlers
	advisories_fs := rwos.DirFS(a.AdvisoryDir)
	a.Logger.Sugar().Debugf("building advisory repo index")
	advisory_cfgs, err := advisoryconfigs.NewIndex(advisories_fs)
	if err != nil {
		return err
	}
	a.AdvisoryIndex = advisory_cfgs
	a.Logger.Sugar().Debugf("advisory repo update finished")
	return nil
}

func NewAdvisoryRepo(logger *zap.Logger, url string) (*AdvisoryRepo, error) {
	repo := AdvisoryRepo{
		Logger:      logger,
		URL:         url,
		AdvisoryDir: filepath.Join(".", "advisories", "wolfi"),
	}
	repo.Logger.Sugar().Debugf("advisory repo configuration loaded")
	return &repo, nil
}
