package image

import (
	"archive/tar"
	"compress/gzip"
	"context"
	"fmt"
	"io"
	"strings"

	"github.com/spdx/tools-golang/json"
	"github.com/spdx/tools-golang/spdx/v2/v2_3"

	"github.com/containers/image/v5/docker"
	"github.com/containers/image/v5/image"
	"github.com/containers/image/v5/pkg/blobinfocache/memory"
	"github.com/containers/image/v5/types"

	"go.uber.org/zap"
)

const (
	// tar archives strip the first prefix slash
	WolfiSBOMDir string = "var/lib/db/sbom/"
)

type WolfiImage struct {
	Logger   *zap.Logger
	URL      string
	Arch     string
	OS       string
	SBOMs    []v2_3.Document
	Packages []v2_3.Package
}

func (w *WolfiImage) ExtractSBOMs(reader io.ReadCloser) ([]v2_3.Document, []v2_3.Package, error) {
	sboms := make([]v2_3.Document, 0)
	packages := make([]v2_3.Package, 0)
	// untar the layer and check each file to see if it is an SBOM
	defer reader.Close()
	w.Logger.Sugar().Debugf("creating gzip reader for image data")
	gzip_reader, err := gzip.NewReader(reader)
	if err != nil {
		return sboms, packages, err
	}
	w.Logger.Sugar().Debugf("creating tar reader for image data")
	tar_reader := tar.NewReader(gzip_reader)
	for {
		tar_header, err := tar_reader.Next()
		if err != nil && err == io.EOF {
			w.Logger.Sugar().Debugf("tar reader for image reached EOF")
			break
		} else if err != nil {
			w.Logger.Sugar().Warnf("error from tar reader during image processing: %v", err)
			return sboms, packages, err
		}
		// check if this file is within the SBOM dir
		w.Logger.Sugar().Debugf("checking tar file entry %s for an SBOM file", tar_header.Name)
		if strings.HasPrefix(tar_header.Name, WolfiSBOMDir) {
			// is this a JSON file, and is it a regular file?
			w.Logger.Sugar().Debugf("file %s is in Wolfi SBOM directory, checking if it is JSON", tar_header.Name)
			if strings.HasSuffix(tar_header.Name, ".json") && tar_header.Typeflag == tar.TypeReg {
				// we found an SBOM file potentially, parse it
				w.Logger.Sugar().Debugf("file %s is a JSON file in the Wolfi SBOM directory, attempting to process as SPDX", tar_header.Name)
				spdx_doc, err := json.Read(tar_reader)
				if err != nil {
					w.Logger.Sugar().Warnf("could not load SPDX file: %v", err)
					continue
				}
				// append to sbom list
				sboms = append(sboms, *spdx_doc)
				// append package information
				sbom_packages := make([]v2_3.Package, 0)
				for _, sbom_package := range spdx_doc.Packages {
					w.Logger.Sugar().Debugf("adding package %s to package listing from SPDX SBOM %s", sbom_package.PackageName, tar_header.Name)
					sbom_packages = append(sbom_packages, *sbom_package)
				}
				packages = append(packages, sbom_packages...)
			}
		}
	}

	return sboms, packages, nil
}

// download image, and parse SBOM and package lists
func (w *WolfiImage) Update() error {

	sboms := make([]v2_3.Document, 0)
	packages := make([]v2_3.Package, 0)

	ctx := context.Background()
	system_context := types.SystemContext{
		ArchitectureChoice: w.Arch,
		OSChoice:           w.OS,
	}
	image_source := fmt.Sprintf("//%s", w.URL)

	w.Logger.Sugar().Debugf("attempting to parse image source %s", image_source)
	ref, err := docker.ParseReference(image_source)
	if err != nil {
		return fmt.Errorf("failed to parse image source %s: %v", image_source, err)
	}
	w.Logger.Sugar().Debugf("creating image source for %s, with OS %s and arch %s", image_source, w.OS, w.Arch)
	img_src, err := ref.NewImageSource(ctx, &system_context)
	if err != nil {
		return fmt.Errorf("failed to build image source from %s: %v", ref.DockerReference().String(), err)
	}
	w.Logger.Sugar().Debugf("fetching image")
	img, err := image.FromSource(ctx, &system_context, img_src)
	if err != nil {
		w.Logger.Sugar().Errorf("failed to get image ref: %v", err)
		return fmt.Errorf("failed to get image ref: %v", err)
	}
	blob_cache := memory.New()
	for _, oci_layer := range img.LayerInfos() {
		w.Logger.Sugar().Debugf("inspecting OCI object with annotations %v", oci_layer.Annotations)
		reader, _, err := img_src.GetBlob(ctx, oci_layer, blob_cache)
		if err != nil {
			return fmt.Errorf("failed to get image blob: %v", err)
		}
		// get all files under SBOM path
		w.Logger.Sugar().Debugf("extracting any SPDX SBOM content for layer %s", oci_layer.Digest.Hex())
		layer_sboms, layer_packages, err := w.ExtractSBOMs(reader)
		if err != nil {
			return fmt.Errorf("failed to extract SBOM data from %s: %v", oci_layer.Digest.Hex(), err)
		}
		sboms = append(sboms, layer_sboms...)
		packages = append(packages, layer_packages...)
	}
	err = img.Close()
	if err != nil {
		return fmt.Errorf("failed to close image: %v", err)
	}

	w.SBOMs = sboms
	w.Packages = packages
	w.Logger.Sugar().Infof("loaded %d SPDX SBOM files, %d packages, for image %s", len(sboms), len(packages), w.URL)
	return nil
}

func NewWolfiImage(logger *zap.Logger, image_url string, image_arch string, image_os string) (*WolfiImage, error) {
	image := WolfiImage{
		Logger: logger,
		URL:    image_url,
		Arch:   image_arch,
		OS:     image_os,
	}
	image.Logger.Sugar().Debugf("image configuration loaded")
	return &image, nil
}
